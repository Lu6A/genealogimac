<?php

require_once('connection.php');

function addStudent($firstName, $lastName, $promotionYear){
    $cnx = connection();

    $yearId = $cnx->query("SELECT id FROM promotions WHERE year=$promotionYear")->fetch();   
    $yearId = json_encode($yearId); 
    $yearId = json_decode($yearId); 
    $statement = $cnx->prepare('insert into students (id, firstName, lastName, birthDate, promotionId, godFatherId, social, isVisible) VALUES (?, ?, ?, ?, ?, ?, ?, ?)');
/*
    $statement->bindParam(':firstName', $firstName);
    $statement->bindParam(':lastName', $lastName);
    $statement->bindParam(':promotionId', $promotionId);
*/
    $statement->execute([NULL, $firstName, $lastName, NULL, $yearId->id, NULL, '', '1']);
    
    /*if ($statement->execute()) {
        echo "New record created successfully";
      } else {
        echo "Unable to create record";
      }*/

}

function deleteStudent($id){
    $cnx = connection(); 
    $cnx->query("UPDATE students SET isVisible=0 WHERE id=$id");
  }

function getStudent($id){ 
  $cnx = connection();

  $infoStudent=$cnx->query("SELECT * FROM students WHERE id=$id ");
  return $infoStudent->fetchAll(PDO::FETCH_ASSOC);
}

function getStudentsFromPromotion($promoYear) {
  $cnx = connection();

  //the following lines help to automatically get the right promotionId(between 1 and 5) from a promotion name (2020, 2021, 2022 ...)
  $yearId = $cnx->query("SELECT id FROM promotions WHERE year=$promoYear")->fetch();   
  //output from localhost/genealogimac/php/router.php/students/2022
  //Array ( [id] => 3 [0] => 3 )
  $yearId = json_encode($yearId); //{"id":"3","0":"3"}
  $yearId = json_decode($yearId); // gives me an object from which I can extract data
  //print_r($yearId->id); //this will return what is in the 'id' row so 3 in this exemple
  $StudentsFromPromotion = $cnx->query("SELECT * FROM students WHERE promotionId=$yearId->id AND isVisible=1"); //get the students who have promotionId=3 in this specific case but works for all promotions
  return $StudentsFromPromotion->fetchAll();
}

function getDirectGodFather($idStudent) {
  $cnx = connection();
  $godFather = $cnx->query("SELECT parrain.id, parrain.firstName, parrain.lastName FROM students as filleul join students as parrain on filleul.godFatherId=parrain.id where filleul.id=$idStudent");

  if($godFather == false)
  {
    echo "pas de réponse de la table pour cette requête";
  }
  else
  {
    return $godFather->fetchAll(PDO::FETCH_ASSOC);
  }
}


function getDirectGodChild($idStudent)
{
    $cnx = connection();
    $godChild = $cnx->query("SELECT * FROM students WHERE godFatherId = $idStudent");

    if ($godChild == false)
    {
      echo "pas de réponse de la table pour cette requête";
    }
    else
    {
      return $godChild->fetchAll(PDO::FETCH_ASSOC);
    }
    
}

//usefull id we want to continue the website and display more informations (for exemple the promotion of each member of the family)
function getPromotionFromStudent($id){
  $cnx = connection();
  $promoYear = $cnx->query("SELECT year FROM students INNER JOIN promotions ON promotions.id = students.promotionId where students.id=$id");  
  return $promoYear->fetchAll();
}


?>
