/*this file is used for display, you can use it based on this exemple 

function displayFunction(data) {
    var content = "<table><tr><td>Table Name</td></tr>"; // your html to display
    data.forEach(function (element) { //go through the data and for each element do the following
        content += '<tr><td>' + element.attribute + "</td></tr>"; 
        //attribute is the name of the column you want to get, it needs to be the same as in the dataset
    });
    content += "</table>";
    //content += is to accumulate html code in one variable so you can write html more clearly on separate lines 
    document.getElementById('here').innerHTML = content;
    // document.getElementById('here') gets the tag where you put id="here" in the index.html
    // for this you need to create a new empty div in the index.html like
    // <div id="yourId"></div>
    // innerHTML = content is to insert the all the HTML you wrote in the var content in the tag you chose
}
*/


function displayPromotions(promotions) { //on va mettre dans content ce qu'on veut mettre dans la div
    var content = "<h3>Promotions</h3><table>"; //on ouvre le tableau
    promotions.forEach(function (promotion) { //on récupère chaque élément de 'promotions' et on les nomme respectivement 'promotion'
        content += '<tr class="promo"><td><a href="#">' + promotion.year + '</a></td><td>\
        <a href="#">X</a> \
        </td></tr>'; // 'content +=' permet de compléter la ligne html (dans content), on fait une ligne de tableau html pour chque élement. Dans chque ligne, j'ai 2 colonnes 'promo' et 'X'. Les '\' permettent de dire 'ce qui suit est annulé (la fin de la ligne), ça permet d'écrire la suite sur la ligne d'en dessous
    });
    content += "</table><br>"; //on ferme le tableau
    document.getElementById('promotions').innerHTML = content; // 'document' fait référence à index.html. 'getElementById' va chercher dans le html la div (ça peut être n'importe quelle balise) dont l'id est 'promotions'. 'innerHTML' remplace le contenu de la div par 'content';
}



function displayStudentsFromPromo(students, year){
    var content = "<br><h3>Students from "+ year + "</h3>";
    students.forEach(function (student) {
        content += '<br> <button onclick="addStudentInfo(' + student.id + ')"> + </button> '  + student.firstName + ' ' + student.lastName + '<br>';
    });
    content += "<br>";

    document.getElementById('studentsDisplay').innerHTML = content;
    document.getElementById('family').innerHTML = '';
    document.getElementById('godFather').innerHTML = '';
    document.getElementById('godChild').innerHTML = '';
    document.getElementById('studentInfos').innerHTML = "";
}



function addPromoForm(){
    var content = '<br>\
    <form name="addPromo" action="php/router.php/promotions" method="post"> \
    <div> \
        <label for="year">Year :</label> \
        <input type="text" id="year" name="year"> \
    </div><br> \
    <div class="button"> \
        <input type="submit" value="Add" > \
    </div> \
    </form>';

    document.getElementById('promoForm').innerHTML = content;
    document.getElementById('studentsDisplay').innerHTML = '';
    document.getElementById('family').innerHTML = '';
    document.getElementById('godFather').innerHTML = '';
    document.getElementById('godChild').innerHTML = '';
}



async function addStudentInfo(studentId){
    const infos = await getStudentInfos(studentId)
    infos.forEach(info => {    
        let content = "<h3>Profile</h3>";
        content += '<button onclick="removeStudent(' + studentId + ')" style="width:150px;"> Delete Student </button><br>'; 
        
        content += "Name : " + info.firstName + " " + info.lastName + "<br>Birth date : " + info.birthDate;

        content += '<br><br><button class="relativesButton" onclick="displayGodChild('+info.id+')">God Child</button>';
        
        content += '<br><button class="relativesButton" onclick="displayGodFather('+info.id+')">God Father</button>';

        content+= '<br><button class="relativesButton" onclick="displayAllFamily('+info.id+')">See all the family</button>';


        // Remove previous informations from the dom
        document.getElementById('studentsDisplay').innerHTML = '';
        document.getElementById('promoForm').innerHTML = '';
        document.getElementById('family').innerHTML = '';
        document.getElementById('godFather').innerHTML = '';
        document.getElementById('godChild').innerHTML = '';

        // Add the new information in the dom
        document.getElementById('studentInfos').innerHTML = content;
    });
}



function displayAddStudentForm(year){
    var content = '<br>\
    <h3>Add a student to promotion ' + year + ' </h3>\
    <form action="php/router.php/students" method="post"> \
    <div> \
        <label for="firstName">First name :</label> \
        <input type="text" id="firstName" name="firstName" required placeholder="John"> \
    </div><br> \
    <div> \
        <label for="lastName">Last name :</label> \
        <input type="text" id="lastName" name="lastName" required placeholder="Doe"> \
    </div><br> \
    <div> \
        <label for="birthDate">Birth date :</label> \
        <input type="date" id="birthDate" name="birthDate" value="2000-01-01" placeholder="dd/mm/yyyy" required> \
    </div><br>\
    <input type="hidden" id="year" name="year" value="'+ year + '">\
    </div><br> \
    <div class="button"> \
        <button type="submit">Add student</button> \
    </div> \
    </form>';

    document.getElementById('promoForm').innerHTML = content;
}



async function displayGodFather(godChildId)
{
    const godFather = await getGodFather(godChildId);

    let content = '<h4>Godfather :</h4>'

    if (!godFather)
    {
        content = '<p> This student does not have a godfather (yet) :( </p>'
    }
    else
    {
        // console.log(godFather[0].firstName); // on a un tableau dans un tableau
        content += '<p>Name : '+godFather[0].firstName+' '+godFather[0].lastName+'</p>'
    }

    document.getElementById('godFather').innerHTML = content;
}



async function displayGodChild(godfatherId)
{
    const godchild = await getGodChild(godfatherId);
    console.log(godchild[0]);
    let content = '<h4>Godchild : </h4>'

    if (!godchild[0])
    {
        content += '<p> This student does not have a godchild (yet) :( </p>'
    }
    else
    {
        godchild.forEach(child => {
            content += '<p>Name : '+child.firstName+ ' '+child.lastName+'</p>'
        })

    }

    document.getElementById('godChild').innerHTML = content; 
}



async function displayAllFamily(studentId)
{
    const generations = await getAllFamily(studentId);
    
    let content = "<h4> Here is your beautiful family (from babies to grandpas) : </h4>"

    generations.forEach(members => {
        members.forEach(member =>{
            content += '<p>'+member.firstName+' '+member.lastName+' </p>' //'+member.birthDate+' they are pretty much all empty
        });
    })

    document.getElementById('family').innerHTML = content;
}
