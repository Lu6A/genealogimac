document.addEventListener('DOMContentLoaded', async () => {
    //here we call our functions when the page is loaded it's like a main

    //call for the function in the request.js and wait till you have a response(with await) 
    //then call your function in display.js for the layout
    const promotions = await getAllPromotions(); //gives the object 'promotions' as a response
    displayPromotions(promotions); //send the object to display function

    const list = document.getElementsByClassName('promo'); //on va chercher tous les lignes du tableau dont la classe est 'promo' (sachant que faut que la fonction qui remplit les divs soit déjà passé par là) (ie que le tableau soit déjà affiché)
    //get students from a promotion
    for (let element of list) {
        element.firstChild.addEventListener('click', async() => {
            var year = element.firstChild.innerText;
            displayAddStudentForm(year);
            await getAllStudents(year).then(students =>{        
                displayStudentsFromPromo(students, year)
            })
        })
    }

    //delete a promotion
    for (let element of list) { 
        element.lastChild.addEventListener('click', async() => {  
            var year = element.firstChild.innerText; 
            await removePromo(year).then( 
                window.location.reload() 
            )
        })
    }

});