/*

In this file we will write the functions that will make the bridge with our router 
so we can get our data, for this we use async function and here is a model on how to use it

async function myFunction(){
    const response = await fetch ('./php/router.php/endPoint/'); // route path that will call the router.php
    const endPoint = await response.json(); //only continue if we get the promised response
    console.log(endPoint); //check if it works
    return endPoint; //return the data as an object thanks to the routing process (router>controller>model>controller>router>here)
}

*/

async function getAllPromotions(){
    const response = await fetch ('./php/router.php/promotions/');
    const promotions = await response.json();
    console.log("got promotions");
    return promotions;
}


async function getAllStudents(promoYear){
    const response = await fetch ('./php/router.php/students/'+promoYear);
    const students = await response.json();
    console.log(students);
    return students;
}


async function removePromo(promo) {
	const response = await fetch ('./php/router.php/promotions/'+promo, { method: 'DELETE'});
    const promotions = await response.json();
    console.log("year "+ promo+ " was removed" );
    console.log(promotions);
    return promotions;
}

// on définit une fonction asynchrone 
//fetch appelle la route pour aller dans le router, dans promotions, avec la méthode 'DELETE' puis il récupère ce qu'il reçoit
// '+promo' -> concatène la valeur de la promo au lien
// fetch prend en argument le lien et a pour défaut la méthode 'GET' (pour charger une page). Si on veut une autre méthode genre DELETE, il faut mettre un 2nd argument { method: 'DELETE'}
// les console.log pour voir si la fonction a fonctionné


async function getStudentInfos(studentId){
    const response = await fetch ('./php/router.php/student/'+studentId);
    const infos = await response.json();
    //console.log(infos);
    return infos;
}

async function removeStudent(studentId){
    const response = await fetch ('./php/router.php/students/'+studentId, { method: 'DELETE'});
    const array = await response.json();
    var year = array[0]['year'];
    console.log(year);

    //update student from this promo
    const response2 = await fetch ('./php/router.php/students/'+year); 
    const students = await response2.json();
    displayStudentsFromPromo(students, year);
    return getAllStudents(year); //retourne la liste des étudiants de la promo mise à jour
}


async function getGodFather(studentId){
    const response = await fetch ('./php/router.php/godFather/'+studentId);
    const godFather = await response.json();
    console.log(godFather);
    return godFather;
}


async function getGodChild(studentId)
{
    const response = await fetch('./php/router.php/godChild/' +studentId);
    const godchild = await response.json();
    console.log(godchild)
    return godchild;
}


async function getAllFamily(studentId)
{
    const response = await fetch('./php/router.php/family/' + studentId);
    const members = await response.json();
    console.log(members)
    return members;
}
