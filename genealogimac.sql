-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: sqletud.u-pem.fr
-- Generation Time: 10-Maio-2022 às 08:48
-- Versão do servidor: 5.7.30-log
-- PHP Version: 7.0.33-0+deb9u7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dasilvagoncalv_db`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `Promotions`
--

CREATE TABLE `promotions` (
  `id` int(11) NOT NULL,
  `year` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Promotions`
--

INSERT INTO `promotions` (`id`, `year`) VALUES
(1, 2020),
(2, 2021),
(3, 2022),
(4, 2023),
(5, 2024);

-- --------------------------------------------------------

--
-- Estrutura da tabela `Students`
--

CREATE TABLE `students` (
  `id` int(5) NOT NULL,
  `firstName` varchar(30) NOT NULL,
  `lastName` varchar(30) NOT NULL,
  `birthDate` date DEFAULT NULL,
  `promotionId` int(5) DEFAULT NULL,
  `godFatherId` int(5) DEFAULT NULL,
  `social` varchar(50) NOT NULL,
  `isVisible` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `Students`
--

INSERT INTO `students` (`id`, `firstName`, `lastName`, `birthDate`, `promotionId`, `godFatherId`, `social`, `isVisible`) VALUES
(5, 'Myriam', 'Anik', NULL, 1, NULL, '', 1),
(6, 'Marc', 'Blactot', NULL, 1, NULL, '', 1),
(7, 'Noelie', 'Bravo', NULL, 1, NULL, '', 1),
(8, 'Julian', 'Bruxelle', NULL, 1, NULL, '', 1),
(9, 'Audrey', 'Combe', NULL, 1, NULL, '', 1),
(10, 'Emilie', 'Corradi', NULL, 1, NULL, '', 1),
(11, 'Nicolas', 'Cusumano', NULL, 1, NULL, '', 1),
(12, 'Valerian', 'Daul', NULL, 1, NULL, '', 1),
(13, 'Nina', 'de Castro', NULL, 1, NULL, '', 1),
(14, 'Axel', 'Donadio', NULL, 1, NULL, '', 1),
(15, 'Lucas', 'Estla', NULL, 1, NULL, '', 1),
(16, 'Amina', 'Fadili', NULL, 1, NULL, '', 1),
(17, 'Bastien', 'Germain', NULL, 1, NULL, '', 1),
(18, 'Hedi', 'Hamadache', NULL, 1, NULL, '', 1),
(19, 'Lea', 'Harabagiu', NULL, 1, NULL, '', 1),
(20, 'Etienne', 'Juif', NULL, 1, NULL, '', 1),
(21, 'Guillaume', 'Lollier', NULL, 1, NULL, '', 1),
(22, 'Antoine', 'Meresse', NULL, 1, NULL, '', 1),
(23, 'Aubane', 'Marquer', NULL, 1, NULL, '', 1),
(24, 'Olivier', 'Meyer', NULL, 1, NULL, '', 1),
(25, 'David', 'Nasr', NULL, 1, NULL, '', 1),
(26, 'Louise', 'Paris', NULL, 1, NULL, '', 1),
(27, 'Maximilien', 'Pluchard', NULL, 1, NULL, '', 1),
(28, 'Stella', 'Poulain', NULL, 1, NULL, '', 1),
(29, 'Agathe', 'Prioleau', NULL, 1, NULL, '', 1),
(30, 'Jeremy', 'Ratsimandresy', NULL, 1, NULL, '', 1),
(31, 'Nathanael', 'Rovere', NULL, 1, NULL, '', 1),
(32, 'Laurine', 'Sajdak', NULL, 1, NULL, '', 1),
(33, 'Vincent', 'Schmid', NULL, 1, NULL, '', 1),
(34, 'Quetin', 'Sedmi', NULL, 1, NULL, '', 1),
(35, 'Marion', 'Seminel', NULL, 1, NULL, '', 1),
(36, 'Nicolas', 'Senecal', NULL, 1, NULL, '', 1),
(37, 'Florian', 'Torres', NULL, 1, NULL, '', 1),
(38, 'Damien', 'Tolard', NULL, 1, NULL, '', 1),
(39, 'Michel', 'Yip', NULL, 1, NULL, '', 1),
(40, 'Sophie', 'Boyer', NULL, 2, 5, '', 1),
(41, 'Thomas', 'Geslin', NULL, 2, 5, '', 1),
(42, 'Eva', 'Benharira', NULL, 2, 6, '', 1),
(43, 'Zoe', 'Durand', NULL, 2, 7, '', 1),
(44, 'Guillaume', '', NULL, 2, 7, '', 1),
(45, 'Pierre', '', NULL, 2, 8, '', 1),
(46, 'Lucie', 'Lesbats', NULL, 2, 8, '', 1),
(47, 'Maeva', 'Rosenberg', NULL, 2, 9, '', 1),
(48, 'Margaux', 'Seguy', NULL, 2, 9, '', 1),
(49, 'Laura', 'Dietsch', NULL, 2, 10, '', 1),
(50, 'Clara', 'Daigmorte', NULL, 2, 11, '', 1),
(51, 'Roxanne', 'Vallee', NULL, 2, 12, '', 1),
(52, 'Margaux', 'Vaillant', NULL, 2, 13, '', 1),
(53, 'Cyrielle', 'Lasarre', NULL, 2, 13, '', 1),
(54, 'Nicolas', 'Lienart', NULL, 2, 14, '', 1),
(55, 'Antoine', 'Libert', NULL, 2, 15, '', 1),
(56, 'Oceanne', 'Riosset', NULL, 2, 16, '', 1),
(57, 'Line', 'Rathonie', NULL, 2, 17, '', 1),
(58, 'Laurine', 'Lafontaine', NULL, 2, 18, '', 1),
(59, 'Baptiste', 'Ory', NULL, 2, 19, '', 1),
(60, 'Andrea', 'Guillot', NULL, 2, 19, '', 1),
(61, 'Gaelle', 'Vallet', NULL, 2, 20, '', 1),
(62, 'Melissande', 'Gaillot', NULL, 2, 21, '', 1),
(63, 'Alexis', 'Bihan', NULL, 2, 21, '', 1),
(64, 'Mathilde', 'Dumoulin', NULL, 2, 23, '', 1),
(65, 'Yoann', 'Koeppel', NULL, 2, 22, '', 1),
(66, 'Jules', 'Fouchy', NULL, 2, 24, '', 1),
(67, 'Barbara', 'Guyonneau', NULL, 2, 24, '', 1),
(68, 'Amandine', 'Kohlmuller', NULL, 2, 25, '', 1),
(69, 'Julien', 'Haudegond', NULL, 2, 26, '', 1),
(70, 'Martin', '', NULL, 2, 27, '', 1),
(71, 'Laurelenn', 'Sangare', NULL, 2, 27, '', 1),
(72, 'Elisa', 'Ciavaldini', NULL, 2, 28, '', 1),
(73, 'Pierre', 'Thiel', NULL, 2, 29, '', 1),
(74, 'Monica', 'Lisacek', NULL, 2, 30, '', 1),
(75, 'Marco', 'Kouyat', NULL, 2, 31, '', 1),
(76, 'Solenne', 'Mary Vallee', NULL, 2, 31, '', 1),
(77, 'Victorine', 'Maurel', NULL, 2, 32, '', 1),
(78, 'Louisa', 'Chikar', NULL, 2, 33, '', 1),
(79, 'Clelie', 'Chassignet', NULL, 2, 34, '', 1),
(80, 'Johan', 'Boyer', NULL, 2, 35, '', 1),
(81, 'Chapelon', 'Paul', NULL, 2, 36, '', 1),
(82, 'Victor', 'Julien', NULL, 2, 36, '', 1),
(83, 'Amelia', 'Mansion', NULL, 2, 37, '', 1),
(84, 'Sarah', '', NULL, 2, 38, '', 1),
(85, 'Flora', 'Mallet', NULL, 2, 39, '', 1),
(86, 'Alois', '', NULL, 3, 42, '', 1),
(87, 'Brian', '', NULL, 3, 42, '', 1),
(88, 'Laurine', '', NULL, 3, 80, '', 1),
(89, 'Theo', 'Godart', NULL, 3, 40, '', 1),
(90, 'Chloe', '', NULL, 3, 79, '', 1),
(91, 'Emma', '', NULL, 3, 78, '', 1),
(92, 'Esther', '', NULL, 3, 72, '', 1),
(93, 'Lea', 'R', NULL, 3, 50, '', 1),
(94, 'Zoe', '', NULL, 3, 49, '', 1),
(95, 'Fabian', '', NULL, 3, 64, '', 1),
(96, 'Maxime', '', NULL, 3, 43, '', 1),
(97, 'Alaric', '', NULL, 3, 43, '', 1),
(98, 'Benjamin', '', NULL, 3, 66, '', 1),
(99, 'Achraf', '', NULL, 3, 62, '', 1),
(100, 'Vincent', '', NULL, 3, 41, '', 1),
(101, 'Evan', '', NULL, 3, 60, '', 1),
(102, 'Matteo', '', NULL, 3, 67, '', 1),
(103, 'Thomas', '', NULL, 3, 69, '', 1),
(104, 'Luc', '', NULL, 3, 82, '', 1),
(105, 'Nils', '', NULL, 3, 65, '', 1),
(106, 'Theo', 'Gautier', NULL, 3, 65, '', 1),
(107, 'Felix', '', NULL, 3, 68, '', 1),
(108, 'Gregoire', '', NULL, 3, 68, '', 1),
(109, 'Jeanne', '', NULL, 3, 58, '', 1),
(110, 'Fanny', '', NULL, 3, 53, '', 1),
(111, 'Bastien', '', NULL, 3, 46, '', 1),
(112, 'Clothilde', '', NULL, 3, 55, '', 1),
(113, 'Lea', 'L', NULL, 3, 54, '', 1),
(114, 'Charlotte', '', NULL, 3, 74, '', 1),
(115, 'Judicaelle', '', NULL, 3, 85, '', 1),
(116, 'Sterenn', '', NULL, 3, 76, '', 1),
(117, 'Brice', '', NULL, 3, 76, '', 1),
(118, 'Paul', '', NULL, 3, 77, '', 1),
(119, 'Amelie', '', NULL, 3, 59, '', 1),
(120, 'Baptiste', '', NULL, 3, 57, '', 1),
(121, 'Khezi', '', NULL, 3, 57, '', 1),
(122, 'Elo', '', NULL, 3, 56, '', 1),
(123, 'Jeremi', '', NULL, 3, 47, '', 1),
(124, 'Loona', '', NULL, 3, 47, '', 1),
(125, 'Ludwig', '', NULL, 3, 71, '', 1),
(126, 'Justine', '', NULL, 3, 71, '', 1),
(127, 'Theo', 'Dauphin', NULL, 3, 48, '', 1),
(128, 'Erwan', '', NULL, 3, 36, '', 1),
(129, 'Natacha', '', NULL, 3, 52, '', 1),
(130, 'Nelly', '', NULL, 3, 51, '', 1),
(131, 'Alexandre', '', NULL, 3, 61, '', 1),
(132, 'Enguerrand', '', NULL, 3, 84, '', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Promotions`
--
ALTER TABLE `promotions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD KEY `promotionId_FK` (`promotionId`),
  ADD KEY `godFatherId_FK` (`godFatherId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Promotions`
--
ALTER TABLE `promotions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `Students`
--
ALTER TABLE `students`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `Students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `godFatherId_FK` FOREIGN KEY (`godFatherId`) REFERENCES `students` (`id`),
  ADD CONSTRAINT `promotionId_FK` FOREIGN KEY (`promotionId`) REFERENCES `promotions` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
