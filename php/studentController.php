<?php

    require_once('studentModel.php');

    function getDirectGodChildAsJson($idStudent)
    {
        return json_encode(getDirectGodChild($idStudent));
    }

    function getDirectGodFatherAsJson($idStudent)
    {
        return json_encode(getDirectGodFather($idStudent));
    }

    function getStudentsFromPromotionAsJson($promoYear){
        return json_encode(getStudentsFromPromotion($promoYear));
    }
    
    function getStudentAsJson($id){
        return json_encode(getStudent($id));
    }

    function addStudentInDatabase($form) { //form is the data we got from filling the form
       
        $firstName = $form['firstName']; 
        $lastName = $form['lastName'];
        $birthDate = $form['birthDate'];
        $promotionYear = $form['year'];
         
        addStudent($firstName, $lastName, $promotionYear);
        return getStudentsFromPromotionAsJson($promotionYear); 
    }

    function deleteStudentInDatabase($studentId) {
        deleteStudent($studentId); 
        return json_encode(getPromotionFromStudent($studentId));
    }

    function getAllFamilyAsJson($idStudent)
    {
        $family = array();

        //put the current student in the array first
        array_push($family, getStudent($idStudent));

        $directGodchild = getDirectGodChild($idStudent);
        $directGodFather = getDirectGodFather($idStudent);

        
        while ($directGodchild != false)
        {
            //put the godchildren before in the array
            array_unshift($family, $directGodchild);
            $directGodchild = getDirectGodChild($directGodchild[0]["id"]); 
            //in case of multiple godchildren it will only continue the loop with the first one
        }

        while ($directGodFather != false)
        {
            //put the godfather after in the array
            array_push($family, $directGodFather);
            $directGodFather = getDirectGodFather($directGodFather[0]["id"]);
        }

        return json_encode($family);
    }

?>