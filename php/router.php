<?php

require_once('promotionController.php');
require_once('studentController.php');


$request = explode('/',$_SERVER['REQUEST_URI']);
// converts URL into a list of all the elements forming the URL  seaprated by '/' starting from 0 which is localhost/
$method = $_SERVER['REQUEST_METHOD'];
// return the method : GET / POST / DELETE

//Dispatch actions depending on URL
// got to localhost/genealogimac/php/router.php/promotions to check if your routing works
// what is displayed is what you echo here in router.php

switch($request[4]) {
	case 'promotions' : 
		switch($method) {
			case 'GET' : 
				echo getPromotionsAsTable();
				break;
			case 'POST' :
				echo addPromotionInDatabase($_POST);

				//to redirect to main page so the promotions are updated
				$referer = $_SERVER['HTTP_REFERER'];
				header("Location: $referer");
				break;
			case 'DELETE' : //la fonction 'removePromo' du request.js nous amène ici (promotions et méthode DELETE)
				echo deletePromotionInDatabase($request[5]); //echo = afficher ce que retourne 'deletePromotionInDatabase'. on peut mettre qu'un seul echo dans un 'case'
				// $request[5] -> le 6e élément du lien 'localhost/genealogimac/php/router.php/promotions/+promo' -> donc c'est bien la valeur de promo
				break;
			default:
				http_response_code('404');
				echo 'OOPS';
		}
		break;

	case 'students' : 
		switch($method) {
			case 'GET' :
				echo getStudentsFromPromotionAsJson($request[5]);
				break;
			case 'POST' :
				//to redirect to main page 
				$referer = $_SERVER['HTTP_REFERER'];
				header("Location: $referer");

				echo addStudentInDatabase($_POST);
				break;
				
			case 'DELETE' :
				echo deleteStudentInDatabase($request[5]);
				break;
			default:
				http_response_code('404');
				echo 'OOPS';
		}
		break;

	case 'student' : 
		switch($method) {
			case 'GET' :
				echo getStudentAsJson($request[5]);
				break;
			case 'POST' :
				echo addGodFather();
				break;
			case 'DELETE' :
				echo deleteGodFather();
				break;
			default:
				http_response_code('404');
				echo 'OOPS';
				break;
		}
		break;
	case 'godChild':
		switch($method) {
			case 'GET' :
				echo getDirectGodChildAsJson($request[5]);
				break;

			default:
				http_response_code('404');
				echo 'OOPS';
				break;				
		}
		break;
	case 'godFather':
		switch($method) {
			case 'GET' :
				echo getDirectGodFatherAsJson($request[5]);
				break;
		
			default:
				http_response_code('404');
				echo 'OOPS';
				break;				
		}
		break;
	case 'family' :
		switch($method){
			case 'GET' :
				echo getAllFamilyAsJson($request[5]);
				break;

			default:
				http_response_code('404');
				echo 'OOPS';
				break;	
		}
		break;
	default:
		http_response_code('404');
		echo 'OOPS';
		break;

		
}

?>